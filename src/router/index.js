import { createRouter, createWebHistory } from 'vue-router'
import Register from '../components/Register.vue';
import Login from '../components/Login.vue';

const routes = [
    {
        path: '/',
        name: 'guest_dashboard.index',
        component: () => import( /* webpackChunkName: "post.index" */ '@/views/guest/Dashboard.vue')
    },
    {
        path: '/movie/:id',
        name: 'guest_detail',
        component: () => import( /* webpackChunkName: "post.index" */ '@/views/guest/Detail.vue'),
        props: true
    },
    { path: '/register', component: Register },
    { path: '/login', component: Login },
    { 
        path: '/admin/movies',
        name: 'admin_movies.index',
        component: () => import( /* webpackChunkName: "post.index" */ '@/views/admin/Movies.vue')
    },
    { 
        path: '/admin/listUsers',
        name: 'admin_users',
        component: () => import( /* webpackChunkName: "post.index" */ '@/views/admin/User.vue')
    },
    { 
        path: '/admin/listAdmins',
        name: 'admin_admins',
        component: () => import( /* webpackChunkName: "post.index" */ '@/views/admin/Admins.vue')
    }
]

//create router
const router = createRouter({
    history: createWebHistory(),
    routes  // config routes
})

export default router